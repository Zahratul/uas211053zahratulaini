-- phpMyAdmin SQL Dump
-- version 5.3.0-dev+20230105.cdc2f37a1d
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2023 at 06:12 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_website`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id_produk` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kategori` varchar(50) NOT NULL,
  `harga` int(50) NOT NULL,
  `deskripsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `nama`, `kategori`, `harga`, `deskripsi`) VALUES
(1, 'Anting', 'Aksesoris', 45687500, 'My Jewell WE20021 Diamond Earring'),
(2, 'Gelang', 'Aksesoris', 9870000, 'Tiara Infinity Diamond Bracelet'),
(3, 'Cincin Wanita', 'Aksesoris', 100000000, 'C-SJ-C02 South Jewellry Cincin 02'),
(4, 'Cincin Perak Couple', 'Aksesoris', 125000, 'Cincin Couple Pacaran'),
(5, 'Totebag Polos', 'Tas', 30000, 'Totebag Polos kekinian Anak Kuliah'),
(6, 'Celana Kulot', 'Celana', 109000, 'Celana Kulot Jeans Wanita Highwaist'),
(7, 'Gaun Pesta', 'Dress', 200000, 'Gaun Pesta Cantik Gold'),
(8, 'Sepatu Sneakers Wanita', 'Sepatu', 85000, 'Sepatu Sneakers Wanita Hitam Pelangi'),
(9, 'Sepatu Sneakers Pria', 'Sepatu', 150000, 'Sepatu Sneakers Pria LAY BCL 453');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `tanggallahir` date NOT NULL,
  `alamat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama`, `no_hp`, `password`, `tanggallahir`, `alamat`) VALUES
(8, 'Zahratul', '082268957742', '123abc', '2023-02-15', 'Selatpanjang');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
